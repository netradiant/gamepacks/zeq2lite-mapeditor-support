# ZEQ2 Lite map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for [ZEQ2 Lite](https://zeq2.com/lite/).

This gamepack is based on a ZEQ2 Lite game pack shared by BF L:

- https://cdn.discordapp.com/attachments/393174258475466753/871162637487067157/ZEQ2LiteMapKit_2.zip
